#!/bin/sh
#sudo apt-get update && sudo apt-get upgrade

sudo apt install -y x11-xkb-utils xorg

# Config keyboard layout and speed
setxkbmap es
xset r rate 300 50
echo "setxkbmap es" >> "${HOME}/.profile"
echo "xset r rate 300 50" >> "${HOME}/.profile"

# an init system, file mananger, a default terminal, (xset, xrandr), win compositor for transparency, nitrogen for setting a bg..
sudo apt-get install -y xinit pcmanfm xterm lxterminal x11-xserver-utils picom nvidia-utils-515-server gnome-backgrounds nitrogen

# Enpass deps
sudo aptitude install -y libpulse0 libxcb-keysyms1 libxcb-icccm4

# Update nvidia drivers & utils (for some reason they are not the last)
sudo apt install -y nvidia-driver-520 nvidia-utils-520 

# WM: Qtile
sudo apt-get install python3-pip
pip install xcffib
pip install qtile
sudo mkdir -p /usr/share/xsessions
cd /usr/share/xsessions || exit
sudo wget "https://raw.githubusercontent.com/qtile/qtile/master/resources/qtile.desktop"
echo 'PATH="$HOME/.local/bin:$PATH"'

# Update xinit cfg
echo "picom &" >> "${HOME}/.xinitrc"
echo "nitrogen --restore &" >> "${HOME}/.xinitrc"
echo "exec qtile start" >> "${HOME}/.xinitrc"

ssh-add ~/.ssh/id_ed25519 >> '/home/jon/.zshrc'
