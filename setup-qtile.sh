#!/bin/sh
# Set terminal to lxterminal
sed -i 's/terminal = guess_terminal()/terminal = "lxterminal"/' "${HOME}/.config/qtile/config.py"
